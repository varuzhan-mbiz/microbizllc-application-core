import { NgModule } from '@angular/core';
import { CoreModule } from "./src/core.module";
import { ApiUrlConfig } from "./src/services/api-url/api-url.service";

@NgModule( {
    imports: [ CoreModule ],
    exports: [ CoreModule ]
} )

export class MbizCoreModule {

    constructor(){}

    static forRoot(config: ApiUrlConfig):any{
        return CoreModule.forRoot( config );
    }
}

export * from './src/core.module';
