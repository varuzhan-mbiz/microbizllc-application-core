import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from "@angular/http";

import { HttpService }         from "./services/http/http.service";
import {
    ApiUrlService,
    ApiUrlConfig
}        from "./services/api-url/api-url.service";
import { LocalStorageService } from "./services/local-storage/local-storage.service";
import { AuthGuardService }    from "./services/auth-guard/auth-guard.service";

import { DialogSettings }           from "@progress/kendo-angular-dialog/dist/npm/dialog-settings";
import { DialogRef, DialogService } from "@progress/kendo-angular-dialog";

import { LoginComponent } from '@microbizllc/application-login';

@NgModule( {
    imports:      [
        HttpModule,
        CommonModule,
    ],
    declarations: [],
    providers:    [
        ApiUrlService,
        HttpService,
        LocalStorageService,
        AuthGuardService
    ]
} )
export class CoreModule {

    constructor( @Optional() @SkipSelf() parentModule: CoreModule, private dialogService:DialogService) {
        if ( parentModule ) {
            throw new Error(
                'CoreModule is already loaded. Import it in the AppModule only' );
        }
        HttpService.errorOccurred$.subscribe( err => {

            let config: DialogSettings = {};
            let dialogRef: DialogRef;

            switch ( err.status ) {

                case 401: {
                    config = {
                        title:   'Please Login',
                        content: LoginComponent,
                        actions: [ { text: "Cancel" } ]
                    };
                    break;
                }
                case 500: {
                    config = {
                        title:   err.title,
                        content: `${err.detail}. ReportID: ${err.reportid}`,
                    };
                    break;
                }
                default: {
                    config = {
                        title:   err.title,
                        content: err.detail,
                    };
                    break;
                }
            }

            dialogRef = this.dialogService.open( config );

            dialogRef.content && dialogRef.content.instance.onLoggedIn.subscribe( isLoggedIn => {
                isLoggedIn && dialogRef.close();
            } )

        } );
    }

    static forRoot( config: ApiUrlConfig ) {

        return {
            ngModule:  CoreModule,
            providers: [
                { provide: ApiUrlConfig, useValue: config },
                ApiUrlService,
                HttpService,
                LocalStorageService,
                AuthGuardService
            ]
        }

    }

}
export * from "./services/index";
export * from "./types/index";
