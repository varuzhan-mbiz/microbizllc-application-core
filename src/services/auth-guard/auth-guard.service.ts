import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router, ActivatedRoute } from "@angular/router";
import { LocalStorageService } from "../local-storage/local-storage.service";

@Injectable()
export class AuthGuardService {

    constructor( private router: Router, private localStorageService: LocalStorageService ) {

    }

    canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ) {

        return this.createCanActivateObservable( route, state );

    }


    createCanActivateObservable( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ) {

        let path = route.url[ 0 ].path;
        let isLoggedIn = this.localStorageService.getJSON( 'auth' );

        if ( isLoggedIn && path != 'login' ) {

            return true;
        }

        if ( path == 'login' ) {

            if ( isLoggedIn ) {

                this.router.navigate( [ 'brand' ] );
                return false;
            }

            return true;
        }

        this.router.navigate( [ 'login' ], { queryParams: { returnUrl: state.url } } );

    }

}
