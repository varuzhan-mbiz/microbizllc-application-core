import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {

  constructor() { }

  get(key:string){

    return localStorage.getItem( key );

  }

  set(key:string, value: string){

    return localStorage.setItem( key, value );

  }

  getJSON(key:string){

    return this.tryParseAndSafelyReturn( key );

  }

  setJSON(key:string, obj:Object){

    let serializedObject = JSON.stringify( obj );

    this.set( key, serializedObject );

  }

  private tryParseAndSafelyReturn(key){

    let itemInStorageAsString = localStorage.getItem(key);

    let itemInStorage = null;

    try {
      itemInStorage = JSON.parse(itemInStorageAsString);
    }catch (e){
      localStorage.setItem(key, '');
    }

    return itemInStorage;
  }

}
