import { Injectable, ViewChild } from '@angular/core';
import { URLSearchParams, Headers, Http, Response } from "@angular/http";
import { Observable, Subject } from "rxjs";
import { LocalStorageService } from "../index";

@Injectable()
export class HttpService {

  static errorSource = new Subject<any>();

  static errorOccurred$ = HttpService.errorSource.asObservable();

  constructor(
      private http: Http,
      private localStorage: LocalStorageService
  ) {}

  createHeadersWithAuthInfo():Headers {

      let authInfoData:any = this.localStorage.getJSON( 'auth' );

      let headers = new Headers();

      if(!authInfoData) { return headers; }

      let info = `${authInfoData.token_type} ${ authInfoData.access_token }`;

      headers.append( 'Authorization', info );

      return headers;

  }

  get( url: string, data?: URLSearchParams ) {

    let headers = this.createHeadersWithAuthInfo();

    return this.http.get( url, { headers, search: data } )
        .map( this.responseToJSON )
        .catch( this.catchError );

  }

  post( url: string, data?: any ) {

    let headers = this.createHeadersWithAuthInfo();

    return this.http.post( url, data, { headers } )
        .map( this.responseToJSON )
        .catch( this.catchError )

  }

  put( url: string, data: any ) {

    let headers = this.createHeadersWithAuthInfo();

    return this.http.put( url, data, { headers } )
        .map( this.responseToJSON )
        .catch( this.catchError );

  }

  patch( url: string, data: any ) {

    let headers = this.createHeadersWithAuthInfo();

    return this.http.patch( url, data, { headers } )
        .map( this.responseToJSON )
        .catch( this.catchError );

  }

  delete( url: string ) {

    let headers = this.createHeadersWithAuthInfo();

    return this.http.delete( url, { headers } )
        .map( this.responseToJSON )
        .catch( this.catchError );

  }

  private responseToJSON( resp: Response ){

    return resp.json();

  }

  private catchError(err: any){

    let res:any = {};

    try { res = err.json() }catch (e){ res = err; }

    HttpService.errorSource.next( res );

    return Observable.throw( res );

  }

}
