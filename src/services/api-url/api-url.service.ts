export class ApiUrlConfig {
  BASE_URL: string = 'http://not-set';
  LOGIN_URL: string = 'not-set';
}

import { Injectable } from '@angular/core';

@Injectable()
export class ApiUrlService {

  private _baseUrl:string;
  private loginUrl:string;
  private _prefix:string = '/api';

  constructor( private config: ApiUrlConfig) {

    this._baseUrl = config.BASE_URL;
    this.loginUrl = config.LOGIN_URL;

  }

  get brand(){

    return `${this._baseUrl}${this._prefix}/brand`;

  }

  get login(){

      return `${this._baseUrl}${this.loginUrl}`;

  }

}