export interface ApiResponse {
    _embedded:  any,
    _link:      {
        first: { href: string },
        last:  { href: string },
        next:  { href: string },
        self:  { href: string }
    },
    page:        number,
    page_count:  number
    page_size:   number
    total_items: number
}